# Tinc subnet creator

The repository name is self-explanatory: this is a CLI to painlessly create a tinc subnet

## Disclaimer :warning:

I've created this to bypass [terrarform](https://www.terraform.io/intro/index.html) learning curve to start creating a secure Kubernetes cluster.

This CLI does the job quite effectively but you should really look into declarative infrastructure management tools like [terraform](https://www.terraform.io/intro/index.html), [ansible](http://docs.ansible.com/ansible/index.html), etc.

Except for personal issues, I won't maintain this repository but PRs are welcome.

## Installation

### Dependencies

- python 3.6
- virtualenv

### Initialization

```bash
virtualenv -p /usr/local/bin/python3 env
. env/bin/activate
pip install -r requirements.txt
make init
```

### Configuration

I take for granted that you use one and only one SSH key to provision your servers.

Copy/paste your private SSH key to `assets/ssh/server-provider` (which is the filename).

## Getting started

### Add a master

```bash
# ./subnet add master <public_ip_address>
./subnet add master 38.13.215.34
```

### Add a node

```bash
# ./subnet add node <custom_server_name> <public_ip_address>
./subnet add master somefancyname 38.13.215.35
```

### Check it works

```bash
ssh 38.13.215.35
ping 10.0.0.1 # Ping the master

PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
64 bytes from 10.0.0.1: icmp_seq=1 ttl=64 time=9.51 ms
64 bytes from 10.0.0.1: icmp_seq=2 ttl=64 time=7.53 ms
```

Boom :boom: you're done! :tada:

### Getting help

```bash
./subnet --help
```

## Known issues

- No user-friendly message when you have not completed the initialization steps
- No warning when redefining a master or a node