.SILENT:

init:

	if [ ! -f assets/nodes.json ] ; \
	then \
		 echo '[]' > assets/nodes.json ; \
	fi;

	if [ ! -f assets/master.json ] ; \
	then \
		 echo '{}' > assets/master.json ; \
	fi;

	echo 'Add node IPs in assets/nodes'
	echo 'Add master IP in assets/master'
	echo 'Copy your servers (careful, only one) private SSH key in assets/ssh/server-provider'