#!/usr/bin/env python3
# coding: utf-8

from . tinc import TincConfigManager, TincConfigTemplateEngine
from . cluster import Node


class Strategy:

    def __init__(self, master: Node, node: Node) -> None:
        self._master = master
        self._node = node

    def master_public_ip(self) -> str:
        raise NotImplementedError("Should have implemented this")

    def tinc_config(self) -> str:
        raise NotImplementedError("Should have implemented this")

    def definition(self) -> str:
        raise NotImplementedError("Should have implemented this")

    def tinc_config_path(self) -> str:
        raise NotImplementedError("Should have implemented this")

    def tinc_definition_path(self) -> str:
        raise NotImplementedError("Should have implemented this")

    def tinc_master_definition_path(self) -> str:
        raise NotImplementedError("Should have implemented this")

    def tinc_up(self) -> str:
        raise NotImplementedError("Should have implemented this")

    def is_master_association_needed(self) -> str:
        raise NotImplementedError("Should have implemented this")

    def is_master_daemon_restart_needed(self) -> bool:
        raise NotImplementedError("Should have implemented this")

    @classmethod
    def tinc_down(cls):
        return TincConfigTemplateEngine.node_tinc_down()


class NodeStrategy (Strategy):

    def master_public_ip(self) -> str:
        return str(self._master.public_ip())

    def tinc_config(self) -> str:
        return TincConfigTemplateEngine.node_tinc_config(
            self._node.name(),
            self._master.name())

    def definition(self) -> str:
        return TincConfigTemplateEngine.node_definition(self._node.subnet_mesh_interface())

    def tinc_config_path(self) -> str:
        return TincConfigManager.config_path()

    def tinc_definition_path(self) -> str:
        return TincConfigManager.definition_path(self._node)

    def tinc_master_definition_path(self) -> str:
        return TincConfigManager.master_definition_path(self._master.name())

    def tinc_up(self) -> str:
        return TincConfigTemplateEngine.node_tinc_up(
            self._node.subnet_interface())

    def is_master_association_needed(self) -> bool:
        return True

    def is_master_daemon_restart_needed(self) -> bool:
        return True


class MasterStrategy (Strategy):

    def master_public_ip(self) -> str:  # Todo: Refacto to have the right abstraction
        raise RuntimeError("Cannot call this on MasterStrategy")

    def tinc_config(self) -> str:
        return TincConfigTemplateEngine.master_tinc_config(self._node.name())

    def definition(self) -> str:
        return TincConfigTemplateEngine.master_definition(
            self._node.subnet_mesh_interface(),
            self._node.public_ip())

    def tinc_config_path(self) -> str:
        return TincConfigManager.config_path()

    def tinc_definition_path(self) -> str:
        return TincConfigManager.definition_path(self._node)

    def tinc_master_definition_path(self) -> str:  # Todo: Refacto to have the right abstraction
        raise RuntimeError("Cannot call this on MasterStrategy")

    def tinc_up(self) -> str:
        return TincConfigTemplateEngine.node_tinc_up(
            self._node.subnet_interface())

    def is_master_association_needed(self) -> bool:
        return False

    def is_master_daemon_restart_needed(self) -> bool:
        return False
