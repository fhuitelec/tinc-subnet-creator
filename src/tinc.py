#!/usr/bin/env python3
# coding: utf-8

from ipaddress import IPv4Interface, IPv4Address
from string import Template
from io import StringIO

from . cluster import Node


class TincConfigManager:  # Todo: merger car trop de redondance
    _config_path = '/etc/tinc/rancher/'
    _host_sub_dir = 'hosts/'
    _conf_filename = 'tinc.conf'
    _boot_path = '/etc/tinc/nets.boot'
    _vpn_name = 'rancher'
    _tinc_up_filename = 'tinc-up'
    _tinc_down_filename = 'tinc-down'

    @classmethod
    def root_path(cls) -> str:
        return cls._config_path

    @classmethod
    def hosts_dir_path(cls) -> str:
        return cls._config_path + cls._host_sub_dir

    @classmethod
    def config_path(cls) -> str:
        return cls._config_path + cls._conf_filename

    @classmethod
    def boot_path(cls) -> str:
        return cls._boot_path

    @classmethod
    def vpn_name(cls) -> str:
        return cls._vpn_name

    @classmethod
    def definition_path(cls, node: Node) -> str:
        return cls._config_path + cls._host_sub_dir + node.name()

    @classmethod
    def master_definition_path(cls, master_name: str) -> str:
        return cls._config_path + cls._host_sub_dir + master_name

    @classmethod
    def tinc_up_path(cls) -> str:
        return cls._config_path + cls._tinc_up_filename

    @classmethod
    def tinc_down_path(cls) -> str:
        return cls._config_path + cls._tinc_down_filename


class TincConfigTemplateEngine:
    # General
    _tinc_up_path = 'assets/template/tinc-up'
    _tinc_down_path = 'assets/template/tinc-down'
    # Nodes
    _node_tinc_conf_path = 'assets/template/node/tinc.conf'
    _node_definition_path = 'assets/template/node/node_template'
    # Master
    _master_tinc_conf_path = 'assets/template/master/tinc.conf'
    _master_definition_path = 'assets/template/master/master_template'

    @classmethod
    def master_tinc_config(cls, master_name) -> str:
        templated_conf = []

        for line in open(cls._master_tinc_conf_path, 'r').readlines():
            templated_line = Template(line).substitute(master_name=master_name)
            templated_conf.append(templated_line)

        return StringIO(''.join(templated_conf))

    @classmethod
    def node_tinc_config(cls, node_name, master_name) -> str:
        templated_conf = []

        for line in open(cls._node_tinc_conf_path, 'r').readlines():
            templated_line = Template(line).substitute(node_name=node_name, master_name=master_name)
            templated_conf.append(templated_line)

        return StringIO(''.join(templated_conf))

    @classmethod
    def node_definition(cls, private_mesh_interface: IPv4Interface) -> str:
        templated_definition = []

        for line in open(cls._node_definition_path, 'r').readlines():
            templated_line = Template(line).substitute(private_interface=private_mesh_interface)
            templated_definition.append(templated_line)

        return StringIO(''.join(templated_definition))

    @classmethod
    def master_definition(cls, private_mesh_interface: IPv4Interface,
                          public_ip: IPv4Address) -> str:
        templated_definition = []

        for line in open(cls._master_definition_path, 'r').readlines():
            templated_line = Template(line).substitute(
                private_interface=private_mesh_interface,
                public_ip=public_ip)
            templated_definition.append(templated_line)

        return StringIO(''.join(templated_definition))

    @classmethod
    def node_tinc_up(cls, private_interface: IPv4Interface) -> str:
        templated_definition = []

        for line in open(cls._tinc_up_path, 'r').readlines():

            templated_line = Template(line).substitute(
                private_ip=private_interface.ip,
                netmask=private_interface.netmask)
            templated_definition.append(templated_line)

        return StringIO(''.join(templated_definition))

    @classmethod
    def node_tinc_down(cls) -> str:
        return StringIO(''.join(open(cls._tinc_down_path, 'r').readlines()))
