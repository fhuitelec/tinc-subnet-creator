#!/usr/bin/env python3
# coding: utf-8

import json
from ipaddress import IPv4Address, IPv4Interface
from typing import List
import re


class Node:  # Todo: add scaleway private IPs to enhance performance (use subnet_ip instead)
    _subnet_mask = '/24'
    _subnet_mesh_mask = '/32'

    def __init__(self, name: str, public_ip: str, subnet_ip: str):

        super().__init__()
        if not re.match("^[A-Za-z0-9]*$", name):
            raise ValueError('Node name must contain letters and numbers only')

        self._name = name
        self._public_ip = IPv4Address(public_ip)
        self._subnet_interface = IPv4Interface(subnet_ip + self._subnet_mask)
        self._subnet_mesh_interface = IPv4Interface(subnet_ip + self._subnet_mesh_mask)

    def name(self) -> str:
        return self._name

    def public_ip(self) -> IPv4Address:
        return self._public_ip

    def subnet_interface(self) -> IPv4Interface:
        return self._subnet_interface

    def subnet_mesh_interface(self) -> IPv4Interface:
        return self._subnet_mesh_interface


class NodeRepository:

    def __init__(self, master_definition_path, _nodes_definition_path):
        self._nodes_definition_path = _nodes_definition_path
        self._master_definition_path = master_definition_path

    def get_master(self):
        try:
            with open(self._master_definition_path, 'r') as master_definition_file:
                master_definition = json.load(master_definition_file)
                return self._create_node_from_raw(master_definition)

        except (ValueError, KeyError):
            raise ValueError('Master node definition in %s is empty or corrupted. '
                             'You might want to add a master first.'
                             % self._master_definition_path)

    def get_nodes(self) -> List:
        try:
            with open(self._nodes_definition_path, 'r') as nodes_definition_file:
                node_definitions = json.load(nodes_definition_file)

                return list(map(
                    self._create_node_from_raw,
                    node_definitions
                ))
        except ValueError:
            raise ValueError('Node definitions are corrupted. '
                             'You can bypass this by putting "{}" (without quotes) in %s.'
                             % self._nodes_definition_path)

    def save_master(self, master: Node):
        node_encoder = NodeEncoder()

        with open(self._master_definition_path, 'w') as master_definition_file:
            json.dump(
                node_encoder.node_to_dict(master),
                master_definition_file,
                indent=4
            )

    def save_node(self, node: Node):
        node_encoder = NodeEncoder()

        with open(self._nodes_definition_path, 'r') as nodes_definition_file:
            nodes_dict = json.load(nodes_definition_file)

        nodes_dict.append(node_encoder.node_to_dict(node))

        with open(self._nodes_definition_path, 'w') as nodes_definition_file:
            json.dump(
                nodes_dict,
                nodes_definition_file,
                indent=4
            )

    @classmethod
    def _create_node_from_raw(cls, raw_node: [str]):
        return Node(raw_node['name'],
                    raw_node['public_ip'],
                    raw_node['subnet_interface'])


class NodeEncoder:

    def node_to_dict(self, node: Node):
        return {
            'name': node.name(),
            'public_ip': str(node.public_ip()),
            'subnet_interface': str(node.subnet_interface().ip)
        }

    def decode(self, json_object: dict):
        return Node(
            json_object['name'],
            json_object['public_ip'],
            json_object['subnet_interface']
        )

class ClusterNodeProvider:
    _subnet_template = '10.0.0.%i'

    def __init__(self, node_repository: NodeRepository):

        self._node_repository = node_repository

    def create_node(self, name: str, public_ip: str):
        return Node(name,
                    public_ip,
                    self._subnet_template % (self._host_number() + 1))

    def create_master(self, public_ip: str):
        return Node('master',
                    public_ip,
                    self._subnet_template % 1)

    def master(self):
        return self._node_repository.get_master()

    def nodes(self) -> List:
        return self._node_repository.get_nodes()

    def save_master(self, master: Node):
        self._node_repository.save_master(master)

    def save_node(self, node: Node):
        self._node_repository.save_node(node)

    def master_public_ip(self):
        return self.master().public_ip()

    def node_public_ips(self) -> [str]:
        return [node.public_ip() for node in self.nodes()]

    def _host_number(self) -> int:
        return len(self.nodes()) + 1
