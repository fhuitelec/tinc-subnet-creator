#!/usr/bin/env python3
# coding: utf-8
from string import Template

from fabric.context_managers import settings
from fabric.contrib.files import append
from fabric.operations import run, put, get
from io import BytesIO, StringIO

from . strategy import Strategy
from . tinc import TincConfigManager


class Initializer:

    def __init__(self):
        pass

    @classmethod
    def initialize(cls):
        with settings(warn_only=True):
            tinc_is_installed = run('which tincd', ).return_code > 0

        if tinc_is_installed:
            run('apt-get update && apt-get install -y tinc')
        run('mkdir -p %s' % TincConfigManager.root_path())
        run('mkdir -p %s' % TincConfigManager.hosts_dir_path())


class TincConfigurator:  # Todo: Refactor to put all of this in the strategy
    _generate_keys_prompt_text = \
        'Please enter a file to save private RSA key to [/etc/tinc/$vpn_name/rsa_key.priv]: '

    def __init__(self, strategy: Strategy) -> None:
        self._strategy = strategy

    def configure(self):
        put(self._strategy.tinc_config(), self._strategy.tinc_config_path())
        put(self._strategy.definition(), self._strategy.tinc_definition_path())

        self._generate_key()
        self._create_binaries(self._strategy)

    def _generate_key(self):
        prompt_text = Template(self._generate_keys_prompt_text).substitute(
            vpn_name=TincConfigManager.vpn_name())

        with settings(prompts={prompt_text: ''}):
            run('tincd -n %s -K4096' % TincConfigManager.vpn_name())

    @classmethod
    def _create_binaries(cls, strategy: Strategy):
        put(strategy.tinc_up(), TincConfigManager.tinc_up_path())
        run('chmod +x %s' % TincConfigManager.tinc_up_path())

        put(strategy.tinc_down(), TincConfigManager.tinc_down_path())
        run('chmod +x %s' % TincConfigManager.tinc_down_path())

    def associate_to_master(self):
        if self._strategy.is_master_association_needed():
            self._link_node_to_master()
            self._link_master_to_node()

    def _link_node_to_master(self):
        tinc_node_definition = BytesIO()
        get(self._strategy.tinc_definition_path(), tinc_node_definition)
        content = tinc_node_definition.getvalue().decode('UTF-8')

        with settings(host_string=self._strategy.master_public_ip()):
            put(StringIO(content), self._strategy.tinc_definition_path())

    def _link_master_to_node(self):
        with settings(host_string=self._strategy.master_public_ip()):
            tinc_master_definition = BytesIO()
            get(self._strategy.tinc_master_definition_path(),
                tinc_master_definition)
            content = tinc_master_definition.getvalue().decode('UTF-8')

        put(StringIO(content), self._strategy.tinc_master_definition_path())


class TincDaemon:

    def __init__(self, strategy: Strategy):
        self._strategy = strategy

    @classmethod
    def configure(cls):
        tinc_boot_content = BytesIO()
        get(TincConfigManager.boot_path(), tinc_boot_content)

        if TincConfigManager.vpn_name() not in tinc_boot_content.getvalue().decode('UTF-8').splitlines():
            append(TincConfigManager.boot_path(), TincConfigManager.vpn_name())

    def launch(self):
        run('service tinc restart')

        if self._strategy.is_master_daemon_restart_needed():
            with settings(host_string=str(self._strategy.master_public_ip())):
                run('service tinc restart')
